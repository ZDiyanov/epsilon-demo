import configs from '@/configs';

export const plugin = {
  install: (app) => {
    // app.config.globalProperties.$config = configs;
    app.provide('$config', configs);
  },
};

export default plugin;
