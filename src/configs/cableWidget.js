export const columns = [
  {
    type: 'name',
    label: 'Name',
    value: 'name',
  },
  {
    type: 'fromPointer',
    label: 'From',
    value: 'fromPort',
  },
  {
    type: 'toPointer',
    label: 'To',
    value: 'toPort',
  },
];

export default { columns };
