export const columns = [
  {
    type: 'type',
    label: 'Type',
    value: null,
  },
  {
    type: 'fromPointer',
    label: 'From',
    value: null,
  },
  {
    type: 'toPointer',
    label: 'To',
    value: null,
  },
  {
    type: 'status',
    label: 'Status',
    value: null,
  },
  {
    type: 'action',
    label: 'Action',
    value: null,
  },
];

export default { columns };
