export const columns = [
  {
    type: 'port',
    label: 'Port Name',
    value: null,
  },
  {
    type: 'connectedTo',
    label: 'Connected to',
    value: null,
  },
  {
    type: 'cable',
    label: 'Cable',
    value: null,
  },
  {
    type: 'action',
    label: 'Action',
    value: null,
  },
];

export default { columns };
