import { createRouter, createWebHistory } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import { testGuard } from '@/router/traps';
import HomeView from '@/views/Home';

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        slug: 'view_home',
        auth: false,
        permissions: [],
      },
      component: HomeView,
    },
  ],
});

router.beforeEach(multiguard([testGuard]));

export default router;
