import { uid } from 'uid';

export class Job {
  constructor({ typeId = 1, meta: [fromPointer = null, toPointer = null], relatedId = null, isCompleted = false, isActive = true }) {
    this.id = uid();
    this.jobId = `${fromPointer.deviceId}-${toPointer.deviceId}`;
    this.typeId = typeId;
    this.fromPointer = fromPointer;
    this.toPointer = toPointer;
    this.relatedId = relatedId;
    this.isCompleted = isCompleted;
    this.isActive = isActive;
  }

  setJobStatus() {
    this.isCompleted = !this.isCompleted;
  }
}

export default Job;
