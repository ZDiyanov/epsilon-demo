export class Port {
  constructor({ portIndex = 0, job = null }, deviceId) {
    this.portId = `${deviceId}-p${portIndex}`;
    this.name = this.portId.toUpperCase();
    this.portIndex = portIndex;
    this.job = job;
  }
}

export default Port;
