import { reactive } from 'vue';
import { Port } from '@/classes/portClass';

export class Device {
  constructor({ name = '', ports = [] }) {
    this.deviceId = name;
    this.name = name.toUpperCase();
    this.ports = reactive(
      ports
        .map((item) => new Port(item, name))
        .sort((a, b) => a.portIndex - b.portIndex)
    );
  }

  displayPortToPointer(deviceId, portIndex) {
    const { fromPointer, toPointer } = this.ports[portIndex].job;
    const pointer = [fromPointer, toPointer].find((item) => item.deviceId !== deviceId);

    return `${pointer.deviceId.toUpperCase()}-P${pointer.portIndex}`;
  }

  displayPortCable(portIndex) {
    const { fromPointer, toPointer } = this.ports[portIndex].job;
    return `${fromPointer.deviceId.toUpperCase()}-P${fromPointer.portIndex}-${toPointer.deviceId.toUpperCase()}-P${toPointer.portIndex}`;
  }
}

export default Device;
