import { createApp } from 'vue';
import { router } from '@/router';
import { pinia } from '@/stores';
import { config, head } from '@/plugins';

import App from './App';
import '@/assets/scss/base.scss';

const app = createApp(App);

app.use(router);
app.use(pinia);
app.use(config);
app.use(head);

router.isReady()
  .then(() => app.mount('#app'))
  .catch(() => console.log('Unable to mount VueJS app'));
