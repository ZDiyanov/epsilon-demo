import { markRaw } from 'vue';
import { createPinia } from 'pinia';
import { createPersistedState } from 'pinia-plugin-persistedstate';
import { router } from '@/router';

export const pinia = createPinia();

pinia.use(({ store }) => {
  store.$router = markRaw(router);
});

pinia.use(createPersistedState({
  storage: localStorage,
  key: (store) => `epsilon-persisted__${store}`,
}));

export default pinia;
