import { defineStore } from 'pinia';
import { isObj, isArr, } from '@/utils';
import { jobs as initialState } from '@/stores/initialState';
import { jobs as persist } from '@/stores/persistedState';

/**
 * @description Is valid
 * @param jobs
 * @returns {boolean}
 */
const isValid = (jobs) => isObj(jobs)
  && isArr(jobs.items);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = (initialState) => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial jobs store state');
  }

  const { items } = initialState;
  return () => ({
    items,
  });
};

const getters = {
  getJobs: ({ items }) => items,
};

const actions = {};

export const jobStore = defineStore('jobs', {
  state: initState(initialState),
  getters,
  actions,
  persist,
});

export default jobStore;
