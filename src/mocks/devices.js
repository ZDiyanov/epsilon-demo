export const mockedDevices = [
  {
    id: 1,
    name: 'abc1',
    ports: [
      { id: 1, portIndex: 0 },
      { id: 2, portIndex: 1 },
    ],
  },
  {
    id: 2,
    name: 'xyz9',
    ports: [
      { id: 3, portIndex: 0 },
      { id: 4, portIndex: 1 },
      { id: 5, portIndex: 2 },
      { id: 6, portIndex: 3 },
    ],
  },
  {
    id: 3,
    name: 'mnl5',
    ports: [
      { id: 7, portIndex: 0 },
      { id: 8, portIndex: 1 },
    ],
  },
];

export default mockedDevices;
